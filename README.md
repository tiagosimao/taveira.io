# taveira.io
As seen running at [taveira.io](https://taveira.io)

## How to run locally
This is a [React.js](https://reactjs.org/) application, it requires [npm](https://www.npmjs.com/get-npm) to be installed in your machine

Other than that, just clone the repo and run this
```sh
npm run start
```

## How to run it like a pro
In some server where you can get HTTP traffic to, make sure you have [Docker](https://docs.docker.com/install/) and some version of [Make](https://www.gnu.org/software/make/) and run this
```sh
make install
```
This will build the React.js application in production mode, stuff it in a minimal [NGINX](https://www.nginx.com/) image and run it as a static site container

The website will be functional at the port 4001, regardless of the incoming host or source IP. TLS termination, port forwarding, DNS and other real-world nuisances are out of scope of this project