install:
	docker build -t build.taveira.io .
	docker stop taveira.io || true
	docker container rm taveira.io || true
	docker tag build.taveira.io taveira.io
	docker run --name taveira.io -p 4001:80 --restart=always -d taveira.io

update: pull install

pull:
	git pull

