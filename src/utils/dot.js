import DP from 'dotparser'

function loadFromDot(dotAsText) {
    const result = []
    const dotAsObj = DP(dotAsText)
    if (dotAsObj &&
        dotAsObj.length > 0 &&
        (dotAsObj[0].type === 'digraph' || dotAsObj[0].type === 'graph') &&
        dotAsObj[0].children) {
        const nodes = {}
        dotAsObj[0].children.forEach(child => {
            if (child.type === 'node_stmt') {
                const id = child.node_id.id.toString()
                let label = id
                child.attr_list.forEach(attr => {
                    if (attr.id === 'label') {
                        label = attr.eq
                    }
                })
                const node = createNode(id, label)
                nodes[node.id] = node
            } else if (child.type === 'edge_stmt') {
                const fromId = child.edge_list[0].id.toString()
                const toId = child.edge_list[1].id.toString()
                if (!nodes[fromId]) {
                    nodes[fromId] = { data: { id: fromId, "name": fromId } }
                }
                if (!nodes[toId]) {
                    nodes[toId] = { data: { id: toId, "name": toId } }
                }
                result.push({
                    data: {
                        source: fromId,
                        target: toId
                    }
                })
            }
        });
        for (let key in nodes) {
            result.push(nodes[key])
        }
    }
    return result
}

export function createNode(id, label) {
    if (!id){
        id = label
    }
    return {
        data: {
            id: id,
            "name": label
        }
    }
}

export default loadFromDot