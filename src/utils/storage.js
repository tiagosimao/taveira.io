function update(key, json) {
    localStorage.setItem(key, JSON.stringify(json))
}

function loadMany(key) {
    const read = localStorage.getItem(key)
    try {
        const json = JSON.parse(read);
        return Array.isArray(json) ? json : []
    } catch (e) {
        return [];
    }
}

export function persistRecentFile(file) {
    if(!file || !file.name){
        return
    }
    let existing = loadMany('recent-files')
    existing = existing.filter(f => f.name !== file.name)
    existing.unshift(file)
    update('recent-files', existing)
}

export function listRecentFiles() {
    return loadMany('recent-files')
}

export function loadRecentFile(fileName) {
    const found = loadMany('recent-files').filter(f=>f.name===fileName)
    return found.length > 0 ? found[0] : undefined
}

export function deleteRecentFile(fileName) {
    let existing = loadMany('recent-files')
    existing = existing.filter(f => f.name !== fileName)
    update('recent-files', existing)
}