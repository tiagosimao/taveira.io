import React, { useCallback } from 'react'

import { makeStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';

import { createStore, useStore } from 'react-hookstore';
import loadFromDot from './utils/dot.js'
import { listRecentFiles, loadRecentFile, persistRecentFile, deleteRecentFile } from './utils/storage.js'
import { useDropzone } from 'react-dropzone'
import Editor from './components/Editor.js'
import './App.css'

createStore('workspace', { 'elements': [] })
createStore('selection', {})
createStore('recent-files', listRecentFiles())

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    color: 'black',
    backgroundColor: theme.palette.background.default,
  }
}));

function openRecent(fileName, setWorkspace) {
  const storedFile = loadRecentFile(fileName)
  persistRecentFile(storedFile)
  setWorkspace(storedFile)
}

function loadFiles(acceptedFiles, setWorkspace){
  if (acceptedFiles.length>0){
    const file = acceptedFiles[0]
    file.text().then(read => {
      const loaded = loadFromDot(read)
      const contents = { 
        'name': file.name,
        'lastModified': file.lastModified,
        'size': file.size,
        'type': file.type,
        'elements': loaded }
      persistRecentFile(contents)
      setWorkspace(contents)
    })
  }
}

function App() {

  const classes = useStyles();

  const [workspace, setWorkspace] = useStore('workspace')

  const [recentFiles, setRecentFiles] = useStore('recent-files')

  const onDrop = useCallback(acceptedFiles => {
    loadFiles(acceptedFiles, setWorkspace)
  }, [setWorkspace])
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

  return (
    <div className="App">
      {(workspace && workspace.elements && workspace.elements.length > 0) ?
        <Editor />
          :
          <div>
            <div className="welcome">
              <div className="header">
                <div className="logo rotating">
                </div>
                <div className="title">
                  <h1>taveira.io</h1>
                  <p className="subtitle">online graph visualizer</p>
                </div>
              </div>
              <div className="welcome-body">
                <p className="intro">If you have some DOT (<a href="https://graphviz.org/">Graphviz</a>) files laying around and want to check them out, go ahead and drop them below</p>
                <div className="droparino" {...getRootProps()}>
                  <input {...getInputProps()} />
                  {
                    isDragActive ?
                      <p>You can drop that now</p> :
                      <p><b>Drop a file or click to browse</b></p>
                  }
                </div>
                {
                (recentFiles && recentFiles.length > 0) ?
                <div>
                  <p>or resume previous work:</p>
                    <List className={classes.root} dense={true}>
                    { recentFiles.map(file => 
                      <ListItem button key={file.name} onClick={()=>openRecent(file.name, setWorkspace)}>
                        <ListItemAvatar>
                          <Avatar>
                            <FolderIcon />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary={file.name}
                          secondary={file.lastModified ? new Date(file.lastModified).toLocaleString() : ''}
                        />
                        <ListItemSecondaryAction>
                          <IconButton onClick={()=> {
                                deleteRecentFile(file.name)
                                setRecentFiles(listRecentFiles())
                              }
                            } edge="end" aria-label="delete">
                            <DeleteIcon />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>)
                    }
                  </List>
                </div>
                : ''
                }
                <div className="footer">
                  <p>taveira.io runs 100% on your browser, nothing gets uploaded nowhere nor any tracker is used.
                    The project is fully open sourced and the code is hosted at <a href="https://gitlab.com/tiagosimao/taveira.io">Gitlab.com</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
      }
    </div>
  );
}

export default App;
