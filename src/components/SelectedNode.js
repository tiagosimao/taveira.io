import React from 'react'
import { useStore } from 'react-hookstore';

function SelectedNode() {

    const [selection] = useStore('selection')

    return (
        <div className="selected-node">
            { selection ?
                <p>{ selection['name'] }</p> :
                'no selection'
            }
        </div>
    )
}

export default SelectedNode