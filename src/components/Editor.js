import React, { useState } from 'react'
import Cytoscape from 'cytoscape'
import CytoscapeComponent from 'react-cytoscapejs'
import COSEBilkent from 'cytoscape-cose-bilkent'
import { createStore, useStore } from 'react-hookstore';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import SelectedNode from './SelectedNode.js'
import { createNode } from '../utils/dot.js'

Cytoscape.use(COSEBilkent)

createStore('new-node-name', '')

let cyInstance = undefined

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: '100vh',
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
}));

const theme = createMuiTheme({
    palette: {
        primary: {
            // Purple and green play nicely together.
            main: '#10739E',
        },
        secondary: {
            // This is green.A700 as hex.
            main: '#F0A30A',
        },
    },
});

function Editor() {

    const classes = useStyles();
    
    const [open, setOpen] = React.useState( window.innerWidth > 760 ? true : false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const [workspace, setWorkspace] = useStore('workspace')
    const [selection, setSelection] = useStore('selection')
    const [newNodeName, setNewNodeName] = useStore('new-node-name')

    const reload = () => window.location.reload(false)

    const newNodeNameChanged = e => {
        setNewNodeName(e.target.value)
    }

    const newNodeSubmit = e => {
        try{
            console.log(newNodeName)
            const node = createNode(undefined, newNodeName)
            workspace.elements.push(node)
            cyInstance.add(node)
            cyInstance.layout({ 
                name: 'cose-bilkent',
                fit: true

            }).run()
            setNewNodeName('')
        } finally {
            e.preventDefault()
        }
    }

    return (
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <AppBar
                    position="fixed"
                    className={clsx(classes.appBar, {
                        [classes.appBarShift]: open,
                    })}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            className={clsx(classes.menuButton, open && classes.hide)}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" noWrap>
                            taveira.io
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="persistent"
                    className={classes.drawer}
                    anchor="left"
                    open={open}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                        </IconButton>
                    </div>
                    <Divider />
                    <SelectedNode />
                    <List>
                        <ListItem key='Create'>
                            <form className={classes.root} noValidate autoComplete="off" onSubmit={newNodeSubmit}>
                                <TextField id="create-node-name" label="Create node" value={newNodeName} onChange={newNodeNameChanged}/>
                            </form>
                        </ListItem>
                        <ListItem button key='Exit' onClick={reload}>
                            <ListItemIcon><ExitToAppIcon /></ListItemIcon>
                            <ListItemText primary='Exit' />
                        </ListItem>
                    </List>
                    <Divider />
                </Drawer>
                <main id='editor-content' className={classes.content}>
                    <CytoscapeComponent
                        className="editor-contents"
                        cy={cy=>{
                            if (!cyInstance || cyInstance !== cy){
                                cyInstance = cy
                                cyInstance.on("select", "node", event => {
                                    let clickedOn = event.target
                                    console.log(clickedOn.data())
                                    setSelection(clickedOn.data())
                                    return true
                                })
                                cyInstance.on("unselect", "node", event => {
                                    setSelection({})
                                    return true
                                })
                            }
                        }}
                        stylesheet={[ // the stylesheet for the graph
                            {
                                selector: 'node',
                                style: {
                                    'background-color': '#B1DDF0',
                                    'border-color': '#10739E',
                                    'border-width': 1,
                                    'label': 'data(name)',
                                    'color': 'white'
                                }
                            },
                            {
                                selector: 'edge',
                                style: {
                                    'width': 3,
                                    'line-color': '#10739E',
                                    'target-arrow-color': '#10739E',
                                    'target-arrow-shape': 'triangle',
                                    'line-style': 'solid',
                                    'curve-style': 'bezier'
                                }
                            },
                            {
                                selector: 'node:selected',
                                style: {
                                    'background-color': '#F0A30A',
                                    'border-color': '#BD7000',
                                    'border-width': 1
                                }
                            }
                        ]}
                        elements={workspace.elements}
                        style={{ width: '100%', height: '100%' }}
                        layout={{ name: 'cose-bilkent' }} />
                </main>
            </div>
        </ThemeProvider>)
}

export default Editor