FROM node as builder
COPY . .
RUN npm install && npm run build
FROM nginx:alpine
COPY --from=builder build/ /usr/share/nginx/html